package app

import (
	"context"

	"github.com/graphql-go/graphql"
)

type graphqlHandler struct {
	schema graphql.Schema
	items  []item
}

type item struct {
	ID       string   `json:"id"`
	Name     string   `json:"name"`
	Prices   []price  `json:"prices"`
	UserTags []string `json:"userTags"`
}

type price struct {
	Amount   float64 `json:"amount"`
	Currency string  `json:"currency"`
	Note     string  `json:"note"`
}

func newGraphqlHandler() (*graphqlHandler, error) {
	g := &graphqlHandler{}

	g.items = newItems()

	var priceType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Price",
			Fields: graphql.Fields{
				"amount": {
					Type: graphql.Float,
				},
				"currency": {
					Type: graphql.String,
				},
				"note": {
					Type: graphql.String,
				},
			},
		},
	)

	var itemType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Item",
			Fields: graphql.Fields{
				"id": {
					Type: graphql.String,
				},
				"name": {
					Type: graphql.String,
				},
				"prices": {
					Type: graphql.NewList(priceType),
				},
				"userTags": {
					Type: graphql.NewList(graphql.String),
				},
			},
		},
	)

	var queryType = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Query",
			Fields: graphql.Fields{
				/* Get (read) single product by id
				   http://localhost:8080/product?query={product(id:){name,prices,userTags}}
				*/
				"item": &graphql.Field{
					Type:        itemType,
					Description: "Get item by id",
					Args: graphql.FieldConfigArgument{
						"id": &graphql.ArgumentConfig{
							Type: graphql.String,
						},
					},
					Resolve: func(p graphql.ResolveParams) (interface{}, error) {
						id, ok := p.Args["id"].(string)
						if ok {
							// Find item
							for _, item := range g.items {
								if item.ID == id {
									return item, nil
								}
							}
						}
						return nil, nil
					},
				},
				/* Get (read) product list
				   http://localhost:8080/product?query={list{id,name,prices,userTags}}
				*/
				"list": &graphql.Field{
					Type:        graphql.NewList(itemType),
					Description: "Get product list",
					Resolve: func(params graphql.ResolveParams) (interface{}, error) {
						return g.items, nil
					},
				},
			},
		})

	schema, err := graphql.NewSchema(
		graphql.SchemaConfig{
			Query: queryType,
			//Mutation: mutationType,
		},
	)
	if err != nil {
		return nil, err
	}

	g.schema = schema

	return g, nil
}

func newItems() []item {
	return []item{
		{
			ID:   "token-1",
			Name: "玛格丽特(2016) | JSK",
			Prices: []price{
				{
					Amount:   3000,
					Currency: "JPY",
					Note:     "现货价",
				},
				{
					Amount:   2000,
					Currency: "JPY",
					Note:     "二手价",
				},
			},
			UserTags: []string{
				"护士", "十字架", "纯色", "草莓", "萌款", "神款",
			},
		},
		{
			ID:   "token-2",
			Name: "玛格丽特(2016) | SK",
			Prices: []price{
				{
					Amount:   200,
					Currency: "USD",
					Note:     "现货价",
				},
			},
		},
	}
}

func (g *graphqlHandler) request(ctx context.Context, req string) *graphql.Result {
	return graphql.Do(graphql.Params{
		Schema:        g.schema,
		RequestString: req,
	})
}
