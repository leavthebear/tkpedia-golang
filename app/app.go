package app

import (
	"net/http"
	"os"

	"encoding/json"
	"log"

	"github.com/mnmtanish/go-graphiql"
)

func Start() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "5000"
	}

	f, err := os.Create("/var/log/golang/golang-server.log")
	if err != nil {
		log.Print(err)
	}
	defer f.Close()
	log.SetOutput(f)

	http.HandleFunc("/", graphiql.ServeGraphiQL)

	g, err := newGraphqlHandler()
	if err != nil {
		log.Fatal(err)
	}
	http.HandleFunc("/graphql", func(w http.ResponseWriter, r *http.Request) {
		sendError := func(err error) {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
		}

		req := &graphiql.Request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			sendError(err)
			return
		}

		res := g.request(r.Context(), req.Query)

		if err := json.NewEncoder(w).Encode(res); err != nil {
			sendError(err)
		}
	})

	http.HandleFunc("/scheduled", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "POST" {
			log.Printf("Received task %s scheduled at %s\n", r.Header.Get("X-Aws-Sqsd-Taskname"), r.Header.Get("X-Aws-Sqsd-Scheduled-At"))
		}
	})

	log.Printf("Listening on port %s\n\n", port)
	http.ListenAndServe(":"+port, nil)
}
